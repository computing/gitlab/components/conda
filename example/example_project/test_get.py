from example_project import get_json


def test_get_json(requests_mock):
    # mock the request
    requests_mock.get(
        "https://example.com/get_json",
        json={"message": "Hello World"},
    )

    # test that we get json
    assert get_json(
        "https://example.com/get_json",
    )['message'] == "Hello World"
