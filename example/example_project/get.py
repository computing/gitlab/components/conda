import requests


def get_json(url, **kwargs):
    """Get JSON from a URL.
    """
    resp = requests.get(url, **kwargs)
    resp.raise_for_status()
    return resp.json()
