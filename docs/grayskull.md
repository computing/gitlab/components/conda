# `conda/grayskull`

Configures a job to build a package using `grayskull`.

## Description

This component configures a job to build a Conda package using
[Conda-build](https://docs.conda.io/projects/conda-build/en/stable/) by
creating a recipe on-the-fly using
[Grayskull](https://conda.github.io/grayskull/) and then passing
that recipe to the [`conda/build`](./build.md) component.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/conda/grayskull@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `conda_build_config` | `""` | The conda-forge build config to use (e.g. `linux_64_`) |
| `conda_build_options` { .nowrap } | `""` | Extra options to pass to `conda build` |
| `job_name` | `grayskull` { .nowrap } | The name to give the build job |
| `needs` | `[]` | List of jobs whose artifacts are needed for this job |
| `source_distribution` | `"*.tar.*"` | Path/pattern for source distribution to build from |
| `stage` | `build` | The pipeline stage to add jobs to |

## Customisation

### `.condarc`

The behaviour of `conda` and `conda-build` can be manipulated via a `.condarc`
file in the root of the project repository.

For details on using the `.condarc` configuration file, see
[here](https://docs.conda.io/projects/conda/en/stable/user-guide/configuration/use-condarc.html).

## Examples

### Build a Conda package for a Python project using grayskull {: #python }

To build a Conda package for a Python project with a recipe dynamically
created using Grayskull, add the `conda/grayskull` component alongside the
(existing?) `python/sdist` component to build from the tarball:

!!! example "Build a conda package for a Python project"

    ```yaml
    stages:
      - build
      - conda

    include:
      - component: git.ligo.org/computing/gitlab/components/python/sdist@<VERSION>
        inputs:
          stage: build
      - component: git.ligo.org/computing/gitlab/components/conda/grayskull@<VERSION>
        inputs:
          stage: conda
          needs: [sdist]
    ```
