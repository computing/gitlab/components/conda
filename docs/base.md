# `conda/base`

Configure the `.conda_base` job template to simplify use of
[`conda`](https://conda.io) in a job.

## Description

The `conda/base` component defines the `.conda_base` job (template) with
a pre-configured conda distribution and appropriate configuration for
isolated software environments and package download caching.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/conda/base@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `cache_dir` | `.cache/conda/pkgs` { .nowrap } | The path to cache downloaded conda packages to (relative to `$CI_PROJECT_DIR`) |
| `conda_dist` | `Miniforge3` | Name of the conda distribution to use |
| `need_write_base` { .nowrap } | `false` | Ensure that the conda `base` environment is writable by the job |

## Examples

### Basic usage { #basic }

The `.conda_base` template can be `extend`ed to simplify creating software
environment using conda as follows:

!!! example "Extend the `.conda_base` template"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/conda/base@<VERSION>

    conda_test:
      extend: .conda_base
      script:
        - conda create -n test -y python pytest
        - conda activate test
        - python -m pytest .
    ```
