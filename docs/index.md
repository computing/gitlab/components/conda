# Conda CI/CD components

The `computing/gitlab/components/conda` project provides
[CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html) pipeline
to build a [Conda](https://conda.io) package, or to prepare a software environment
using `conda`.

## Latest release

[![latest badge](https://git.ligo.org/computing/gitlab/components/conda/-/badges/release.svg)](https://git.ligo.org/explore/catalog/computing/gitlab/components/conda/ "See latest release in the CI/CD catalog")

## Components

The following components are available:

- [`conda/base`](./base.md "`conda/base` component documentation")
- [`conda/build`](./build.md "`conda/build` component documentation")
- [`conda/grayskull`](./grayskull.md "`conda/grayskull` component documentation")
