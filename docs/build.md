# `conda/build`

Configures a job to build a package using `conda-build`.

## Description

This component configures a job to build a Conda package using
[Conda-build](https://docs.conda.io/projects/conda-build/en/stable/) by
creating a conda-forge
[feedstock](https://conda-forge.org/docs/maintainer/infrastructure/#feedstocks)
and leveraging the conda-forge build configuration.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/conda/build@<VERSION>
    inputs:
      recipe_dir: conda/
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `conda_build_config` | `""` | The conda-forge build config to use (e.g. `linux_64_`) |
| `conda_build_options` { .nowrap } | `""` | Extra options to pass to `conda build` |
| `job_name` | `conda_build` { .nowrap } | The name to give the build job |
| `needs` | `[]` | List of jobs whose artifacts are needed for this job |
| `recipe_dir` | | Path of directory containing meta.yaml |
| `stage` | `build` | The pipeline stage to add jobs to |

## Customisation

### `.condarc`

The behaviour of `conda` and `conda-build` can be manipulated via a `.condarc`
file in the root of the project repository.

For details on using the `.condarc` configuration file, see
[here](https://docs.conda.io/projects/conda/en/stable/user-guide/configuration/use-condarc.html).

## Examples

### Build a conda package {: #build }

To build a conda package directly from the project using a recipe stored in
the project, add the `conda/build` component as follows:

!!! example "Build a conda package using conda-build"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/conda/build@<VERSION>
        inputs:
          recipe_dir: conda/
          conda_build_options: "--error-overdepending"
    ```

See the
[recipe](https://git.ligo.org/computing/gitlab/components/conda/-/tree/main/example/conda/)
in the `computing/gitlab/components/conda` project for a working example.

### Building multiple packages from a monorepo { #monorepo }

The same component can be included multiple times in the same pipeline with
different inputs to build multiple subpackages of a monorepo, e.g:

!!! example "Build multiple packages for a monorepo"

    ```yaml
    stages:
      - package1
      - package2
      - package3

    include:
      - component: git.ligo.org/computing/gitlab/components/conda/build@<VERSION>
        inputs:
          stage: package1
          recipe_dir: package1/conda
          job_name: conda_build-package1
      - component: git.ligo.org/computing/gitlab/components/conda/build@<VERSION>
        inputs:
          stage: package2
          recipe_dir: package2/conda
          job_name: conda_build-package2
          needs: [conda_build-package1]
      - component: git.ligo.org/computing/gitlab/components/conda/build@<VERSION>
        inputs:
          stage: package3
          recipe_dir: package3/conda
          job_name: conda_build-package3
          needs:
            - conda_build-package1
            - conda_build-package2
    ```
