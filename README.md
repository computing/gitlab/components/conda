# CI/CD components for Conda

This project provides [CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html) pipeline
to build a [Conda](https://conda.io) package, or to prepare a software environment
using `conda`.

[[_TOC_]]

## Usage

For full documentation, see

<https://computing.docs.ligo.org/guide/gitlab/components/conda/>

## Contributing

Please read about CI/CD components and best practices at:
<https://git.ligo.org/help/ci/components/index.html>.

All interactions related to this project should follow the
[LIGO-Virgo-KAGRA Code of Conduct](https://dcc.ligo.org/LIGO-M1900037).

For more details on contributing to this project, see `CONTRIBUTING.md`.
